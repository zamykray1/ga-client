import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestASClient {

    private static String ip = "185.25.119.209";
//    private static String ip = "localhost";
    private static int port = 4455;

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        try(Socket socket = new Socket(ip, port);
            BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
            DataOutputStream oos = new DataOutputStream(socket.getOutputStream());
            DataInputStream ois = new DataInputStream(socket.getInputStream());	)
        {

            System.out.println("Starting auth...");
            oos.writeUTF("{\"path\":\"auth/auth\",\"login\":\"radalex\",\"password\":\"222\"}");
            oos.flush();System.out.println(ois.readUTF());

            System.out.println("Get materials...");
            oos.writeUTF("{\"path\":\"objects/get\",\"location\":{\"lat\":50.405055,\"lon\":30.6190386}}");
            oos.flush();System.out.println(ois.readUTF());

            System.out.println("Collect material...");
            oos.writeUTF("{\"path\":\"materials/collectMaterial\",\"geoMaterialId\":3,\"location\":{\"lat\":50.405055,\"lon\":30.6190386}}");
            oos.flush();System.out.println(ois.readUTF());

            System.out.println("Get collected materials...");
            oos.writeUTF("{\"path\":\"materials/getCollectedMaterials\"}");
            oos.flush();System.out.println(ois.readUTF());

            System.out.println("Check synthesis"); // materials keys - material_id, values - count
            oos.writeUTF("{\"path\":\"materials/checkSynthesis\",\"materials\":{1:1,2:1}}");
            oos.flush();System.out.println(ois.readUTF());

            System.out.println("Synthesize");
            oos.writeUTF("{\"path\":\"materials/synthesize\",\"materials\":{1:1,2:1}}");
            oos.flush();System.out.println(ois.readUTF());

//            while(!socket.isOutputShutdown()){
//
//                if(br.ready()){
//
//                    System.out.println("Client start writing in channel...");
//
//                    String clientCommand = br.readLine();
//
//                    oos.writeUTF(clientCommand);
//                    oos.flush();
//                    System.out.println("Client sent message " + clientCommand + " to server.");
//
//                    if(clientCommand.equalsIgnoreCase("quit")){
//                        System.out.println("Client kill connections");
//                        Thread.sleep(2000);
//
//                        if(ois.available()!=0)		{
//                            System.out.println("reading...");
//                            String in = ois.readUTF();
//                            System.out.println(in);
//                        }
//
//                        break;
//                    }
//
//                    System.out.println("Client wrote & start waiting for data from server...");
//
//                    if(ois.available()!=0) {
//                        String in = ois.readUTF();
//                        System.out.println(in);
//                    }
//                }
//            }

            System.out.println("Closing connections & channels on clientSide - DONE.");

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}